 def whiteout_document_inn(image, output_dir=None, image_name=None, debug=False):
    """
    This method is used to clear the image from unnecessary details
    before text recognition process, this method is universal 
    :param image: original document image. Should be normalized by size and color
    :param output_dir: path to the output storage
    :param image_name: 
    :param debug: special flag 
    :return: new whiteouted image
    """

    kernels = [
        ((3, 1), di),
        ((3, 150), er),
        ((5, 50), di),
        ((5, 150), er),
        ((7, 50), di),
        ((7, 150), er),
        ((7, 150), di),
        ((5, 50), er),
        ((5, 150), di),
        ((5, 50), er),
    ]

    height, width = image.shape[:2]
    grayscale_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    for i in range(10):
        grayscale_image = cv2.GaussianBlur(grayscale_image, (13, 13), 0)
    bw_image = cv2.adaptiveThreshold(grayscale_image, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 15, 2)
    bw_image = remove_borders(bw_image, output_dir, image_name, debug)
    steps = morph_multi_simple_kernel(bw_image, kernels, ret_all_steps=True)
    morphed_img = steps[-1]
    image = rgb2grayrgb(image)

    _, contours, hierarchy = cv2.findContours(morphed_img, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    new_image = create_white_image(height, width)

    # if median of the image inside contour is close to be black we consider this piece is good
    # otherwise the piece is not sufficient to use
    for i, layer_info in enumerate(zip(contours, hierarchy[0])):
        contour = layer_info[0]
        x, y, w, h = cv2.boundingRect(contour)
        morphed_cropped = morphed_img[y:y + h, x:x + w]
        if h < height / 3 and np.mean(morphed_cropped) < 190:
            step = int((1 / 3 * h) / 2)
            new_image[y - step:y + h + step, x:x + w] = image[y - step:y + h + step, x:x + w]
    new_image = cv2.adaptiveThreshold(cv2.cvtColor(new_image, cv2.COLOR_RGB2GRAY), 255, cv2.ADAPTIVE_THRESH_MEAN_C,
                                      cv2.THRESH_BINARY, 127, 75)
    new_image = cv2.cvtColor(new_image, cv2.COLOR_GRAY2BGR)
    return new_image
