import os
from cv2 import MORPH_ERODE as er, MORPH_DILATE as di

import cv2
import numpy as np
from image_operations.image_operations import create_white_image, morph_expand, morph_vh, preprocess_image_and_scale

def detect_multicolumn(image, output_dir=None, image_name=None, debug=False):
    """
    Tries to detect the table on the image, gets it borders and marks the blocks
    of the table to arrange them in horizontally for better text recognition
    :param image: the document to be processed
    :param output_dir: 
    :param image_name: 
    :param debug: special flag to control the output of the intermediate steps like images 
    after morphing and some useful text information
    :return: horizontally arranged blocks of the table on the image surface
    """

    height, width = image.shape[0:2]
    scale_factor = np.floor((4000 / height) * 2 + 0.5) / 2

    vertical_kernels = [
        ((3, 1), er),
        ((1, 15), di),
        ((3, 15), er),
        ((1, 45), di),
        ((20, 20), er)
    ]

    text_expanding_kernels = [
        ((35, 35), er),
    ]

    white_out_kernels = [
        ((1, 10), er),
        ((3, 1), di),
        ((2, 9), er),
        ((1, 15), di),
        ((1, 2), er),
        ((1, 12), di),
        ((1, 4), er),
        ((5, 1), di),
        ((9, 40), er),
        ((3, 1), di)
    ]

    image, preprocessed_image = preprocess_image_and_scale(image, scale_factor, 5)

    height, width = preprocessed_image.shape[0:2]
    vertical_morphed, horizontal_morphed, cross_morphed = morph_vh(preprocessed_image, vertical_kernels)

    expanded_image = morph_expand(preprocessed_image, white_out_kernels, text_expanding_kernels)

    resulting_morphed = 255 - (cross_morphed - expanded_image)
    
    filter_params = {
        'width': width, 
        'height': height, 
        'image': image, 
        'morphed_image':resulting_morphed, 
        'debug': debug, 
        'borders': detect_table_borders(vertical_morphed, horizontal_morphed,width, height))
    }

    image, valid_contours, heights, widths = filter_valid_contours(**filter_params)
    
    if len(valid_contours) < 1:
        return image

    if debug:
        if not output_dir or not image_name:
            raise ValueError("You should specify both output_dir and image_name in debug mode")
        name, ext = os.path.splitext(image_name)
        
        def save_intermediate(prefix, img):
            cv2.imwrite(os.path.join(output_dir, "{}-{}{}".format(name, prefix , ext)), img)
        
        save_intermediate("h", horizontal_morphed)
        save_intermediate("v", vertical_morphed)
        save_intermediate("cross", cross_morphed)
        save_intermediate("bordered", image)
        save_intermediate("expanded", expanded_image)
        save_intermediate("whiteout", resulting_morphed)

    valid_contours.sort(key=lambda contour: contour[1], reverse=True)
    valid_contours = [pair for pair in 
                      zip(valid_contours, np.zeros(len(valid_contours), dtype=np.int) - 1)][:-1]
    valid_contours.reverse()

    # Numerate contours that are about to be on one line
    for i, contour in enumerate(valid_contours):
        if contour[1] == -1:
            x, y, w, h = contour[0][0:4]
            for j, inner_contour in enumerate(valid_contours):
                x_inner, y_inner, w_inner, h_inner = inner_contour[0][0:4]
                if contour[1] == -1 and i != j:
                    if y < y_inner < y + h:
                        valid_contours[j] = (inner_contour[0], i)
            valid_contours[i] = (contour[0], i)
    sorted(valid_contours, key=lambda contour: contour[1])

    # Create a new white image and paste contents of blocks into it
    v_step, h_step = int(width / 200), int(height / 200)
    new_image_height = sum(heights) + (h_step + 10) * (len(heights) + 1)
    new_image_width = max(widths) + (v_step + 10) * (len(widths) + 1)
    new_image = create_white_image(new_image_height, new_image_width)
    init_top_margin, const_left_margin = 10, 10
    for contour in valid_contours:
        x, y, w, h = contour[0][0:4]
        if w < h:
            x -= v_step if x > v_step else 0
            w += v_step
        else:
            y -= h_step if y > h_step else 0
            h += h_step
        new_image[init_top_margin:init_top_margin + h, 
                  const_left_margin:const_left_margin + w] = image[y:y + h, x:x + w]
        init_top_margin += (h + 10)
    return new_image


def detect_table_borders(vertical_morphed_image, horizontal_morphed_image, width, height):
    """ 
    Loop through vertical and horizontal lines detected on the image surface and select
    the most 4 of them so that the square between them is close to be maximum
    Consider the margin from image border to be 10 px

    :param vertical_morphed_image: image that contains only vertical lines from table
    :param horizontal_morphed_image: image that contains only horizontal lines from table
    :param width: image width
    :param height: image height
    :return: left, right, top and bottom borders of the table
    """

    _, v_contours, v_hierarchy = cv2.findContours(vertical_morphed_image, 
                                                  cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    _, h_contours, h_hierarchy = cv2.findContours(horizontal_morphed_image, 
                                                  cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

    most_left, most_right, most_top, most_bottom = width, 0, height, 0

    for i, layer_info in enumerate(zip(v_contours, v_hierarchy[0])):
        contour = layer_info[0]
        x, y, w, h = cv2.boundingRect(contour)
        if most_left > x > 10:
            most_left = x

        if most_right < x + w < width - 10:
            most_right = x + w

    for i, layer_info in enumerate(zip(h_contours, h_hierarchy[0])):
        contour = layer_info[0]
        x, y, w, h = cv2.boundingRect(contour)
        if most_top > y > 10:
            most_top = y

        if most_bottom < y + h < height - 10:
            most_bottom = y + h

    return [int(most_left), int(most_right), int(most_top), int(most_bottom)]


def filter_valid_contours(width, height, image, morphed_image, debug, borders):
    """
    Filters contours of text blocks found on the image 
    :param width: width of the original image
    :param height: height of the original image
    :param image: the document to be processed
    :param morphed_image: the image where text is expanded to be weaved together, 
    everything excess if whiteouted, and the table mask is applied
    :param debug: special flag to control the output of the intermediate steps like images 
    after morphing and some useful text information
    :param borders: possible borders of the table that were detected on previous step of the algorithm
    :return: original image with text blocks rectangles if debug is on, list of valid contours, 
    widths and heights of this contours
    """
    valid_contours, heights, widths = [], [], []
    left, right, top, bottom = borders[0:4]
    _, contours, hierarchy = cv2.findContours(morphed_image, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    for i, layer_info in enumerate(zip(contours, hierarchy[0])):
        contour = layer_info[0]
        if contour is not None:
            next, prev, child, parent = layer_info[1]
            x, y, w, h = cv2.boundingRect(contour)
            if x + w < width and h + y < height:
                for j, contour in enumerate(contours):
                    if i != j:
                        x_inner, y_inner, w_inner, h_inner = cv2.boundingRect(contour)
                        x_c_inner, y_c_inner = int(x_inner + w_inner / 2), int(y_inner + h_inner / 2)
                        if x < x_c_inner < x + w and y < y_c_inner < y + h:
                            contours[j] = None

                x_c, y_c = int(x + w / 2), int(y + h / 2)
                morphed_cropped = morphed_image[y:y + h, x:x + w]
                if left < x_c < right and top < y_c < bottom 
                    and morphed_cropped.max() != morphed_cropped.min() 
                    and x + w < width and h + y < height:
                    valid_contours.append([x, y, int(w), int(h)])
                    heights.append(int(h))
                    widths.append(int(w))

                    if debug:
                        if child == -1:
                            image = cv2.rectangle(image, (x, y), (x + w, y + h), (255, 0, 0), 2)
                        else:
                            image = cv2.rectangle(image, (x, y), (x + w, y + h), (0, 0, 255), 2)
    image = cv2.rectangle(image, (left, top), (right, bottom), (48, 48, 48), 4)
    return image, valid_contours, heights, widths
