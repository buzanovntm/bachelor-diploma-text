 def fix_angle(img, with_drawing=False, return_angle=False):
    (h, w) = img.shape[:2]
    w_limit = 0.95
    contours, most_common_width, img_copy = get_image_characteristics(img, w, w_limit)  
    img_rotated = rotate_img(img, 90, with_text=False)
    contours_rotated, most_common_width_rotated, img_copy_rotated = get_image_characteristics(img_rotated, h, w_limit)
    if most_common_width < most_common_width_rotated:
        img = img_rotated
        contours = contours_rotated
        w = h
        most_common_width = most_common_width_rotated
        img_copy = img_copy_rotated
    angles = []
    widths = []
    for cnt in contours:
        rect = cv2.minAreaRect(cnt)
        box = np.int0(cv2.boxPoints(rect))
        side_long = np.linalg.norm(box[1] - box[0])
        side_short = np.linalg.norm(box[2] - box[1])
        box_max_width = max(side_long, side_short)
        widths.append(box_max_width)
        angle = rect[-1]
        if with_drawing:
            abs(rect[-1]) >= 0.1 and cv2.drawContours(img_copy, [box], 0, (0, 0, 255), 2)
        angles.append(angle)

    w_bins, w_values = np.histogram(widths, bins=max(1, w // 10), range=(0, w))
    bins, values = np.histogram(angles, bins=180, range=(-90, 0))
    
    for i, angle in enumerate(angles):
        angle_bin = int((angle + 90) / 90 * len(bins))
        angle_bin = min(angle_bin, len(bins) - 1)
        width = widths[i]
        if width > w:
            continue
        width_bin = int(widths[i] / w * len(w_bins))
        width_bin = min(width_bin, len(w_bins) - 1)
        bins[angle_bin] += width_bin - 1  
    max_index, max_count = max(enumerate(bins), key=operator.itemgetter(1))
    if max_index == len(bins) - 1:
        if return_angle:
            return img_copy, 0
        else:
            return img_copy
    most_common_angle = (values[max_index] + values[max_index + 1]) / 2
    result_angle = convert_angle(most_common_angle)
    result_image = rotate_img(tmp, result_angle * (-1), with_text=True)
    if return_angle:
        return result_image, result_angle
    else:
        return result_image
