BUILD_NAME = diploma
MASTER_NAME = contents
SRC_DIR = src
BUILD_DIR = build
COMMIT_SHA := $(shell git log --pretty=format:'%h' -1)
MASTER_FN_LYX := $(MASTER_NAME).lyx
MASTER_FN_PDF := $(MASTER_NAME).pdf
BUILD_FN_PDF := $(BUILD_NAME)_$(COMMIT_SHA).pdf
BUILD_FN_ARC := $(BUILD_NAME)_$(COMMIT_SHA).tar.gz

.PHONY: clean pull pdf src all
all: pull clean src pdf
clean:
	@rm -rfd build/*
push: pdf
	git status
	@whiptail --yesno "Push changes to repo?" 20 60 && git push || echo "Won't push"
	
pull:
	@echo "Pulling..."
	@git pull
	@echo $(COMMIT_SHA)
pdf:
	@echo "Gathering pdf..."
	@mkdir -p build
	@lyx --export pdf5 $(SRC_DIR)/$(MASTER_FN_LYX) \
		&& mv $(SRC_DIR)/$(MASTER_FN_PDF) $(BUILD_DIR)/$(BUILD_FN_PDF)
src: 
	@echo "Gathering src..."
	@mkdir -p build
	@pytarpv src src \
		&& mv src.tar.gz $(BUILD_DIR)/$(BUILD_FN_ARC)
run:
	@bash -c "(lyx src/contents.lyx &> /dev/null &)"

